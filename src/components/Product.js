import React from "react";

export default function Product(props) {
  const { product, onAddProduct } = props;
  return (
    <div>
      <img className="small" src={product.src} alt={product.id}></img>
      <h3>{product.name}</h3>
      <div>${product.price}</div>
      <div>
        <button onClick={() => onAddProduct(product)}>Add to Cart</button>
      </div>
    </div>
  );
}

import React from "react";

export default function Cart(props) {
  const { cartItems, onAddProduct, onRemoveProduct } = props;
  const productsPrice = cartItems.reduce((a, c) => a + c.price * c.qty, 0);
  const taxPrice = productsPrice * 0.19;
  const shippingPrice = productsPrice > 50 ? 0 : 10;
  const totalPrice = productsPrice + taxPrice + shippingPrice;
  return (
    <aside className="segment col-1">
      <h2>Cart Items</h2>
      {cartItems.length === 0 && <div>Cart is Empty</div>}
      {cartItems.map((item) => (
        <div key={item.id} className="row">
          <div className="col-2">{item.name}</div>
          <div className="col-2">
            <button onClick={() => onAddProduct(item)} className="add">
              +
            </button>
            <button onClick={() => onRemoveProduct(item)} className="remove">
              -
            </button>
          </div>
          <div className="col-2 text-right">
            {item.qty} x $ {item.price.toFixed(2)}
          </div>
        </div>
      ))}
      {cartItems.length !== 0 && (
        <>
          <hr></hr>
          <div className="row">
            <div className="col-2 text-left">Items Price</div>
            <div className="col-1 text-right">${productsPrice.toFixed(2)}</div>
          </div>
          <div className="row">
            <div className="col-2 text-left">Tax Price</div>
            <div className="col-1 text-right">${taxPrice.toFixed(2)}</div>
          </div>
          <div className="row">
            <div className="col-2 text-left">Shipping Price</div>
            <div className="col-1 text-right">${shippingPrice.toFixed(2)}</div>
          </div>
          <div className="row">
            <div className="col-2 text-left">
              <strong>Total Price</strong>
            </div>
            <div className="col-1 text-right">
              <strong>${totalPrice.toFixed(2)}</strong>
            </div>
          </div>
        </>
      )}
    </aside>
  );
}

import React from "react";
import Product from "./Product.js";

export default function Products(props) {
  const { products, onAddProduct } = props;

  return (
    <main className="segment col-2">
      <h2>Products</h2>
      <div className="row">
        {products.map((product) => (
          <Product
            key={product.id}
            product={product}
            onAddProduct={onAddProduct}
          ></Product>
        ))}
      </div>
    </main>
  );
}

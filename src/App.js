import "./App.css";
import Header from "./components/Header";
import Products from "./components/Products.js";
import Cart from "./components/Cart.js";
import { useState } from "react";
import Data from "./constData";

function App() {
  const [cartItems, setCartItems] = useState([]);
  const onAddProduct = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }]);
    }
  };
  const onRemoveProduct = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !== product.id));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };

  return (
    <div className="App">
      <Header></Header>
      <div className="row">
        <Products
          products={Data.products}
          onAddProduct={onAddProduct}
        ></Products>
        <Cart
          onAddProduct={onAddProduct}
          onRemoveProduct={onRemoveProduct}
          cartItems={cartItems}
        ></Cart>
      </div>
    </div>
  );
}

export default App;

const data = {
  products: [
    {
      id: "1",
      name: "Product A",
      price: 10,
      imgSrc: "",
    },
    {
      id: "2",
      name: "Product B",
      price: 20,
      imgSrc: "",
    },
    {
      id: "3",
      name: "Product C",
      price: 30,
      imgSrc: "",
    },
    {
      id: "4",
      name: "Product D",
      price: 40,
      imgSrc: "",
    },
  ],
};

export default data;

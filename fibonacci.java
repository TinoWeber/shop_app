import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class fibonacci{
    
    public static void main(String[] args) throws IOException{
        System.out.println("\n== Fibonacci - Calculator ==\n");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("How many Fibonacci-Numbers do you want?\n");
        System.out.print("Enter Integer: ");

        int i = 0;
        int a = 0;
        int b = 1;
        boolean isValid=true;
        
        try {
            i = Integer.parseInt(br.readLine());
        } catch(NumberFormatException nfe) {
            System.err.println("Invalid Format!");
            isValid=false;
        } finally{
            if ((i<=0 || i>47) && isValid){
                System.err.println("Invalid Range! Range is [1...47]"); // upper restriction to avoid integer overflow
                isValid = false;
            } 

            if(isValid) {
                System.out.println("You entered: " +i);

                System.out.println("0: 0");
                if(i>1) System.out.println("1: 1");

                for(int j = 1; j<i-1;j++){
                    int c = a +b;
                    a=b;
                    b=c;
                    System.out.println((j+1) + ": " + c);
                }
            }
        }
    }
}